<?php
/**
 * Corporate Pro
 *
 * This file sets up the Corporate Pro Theme.
 *
 * @package   SEOThemes\CorporatePro
 * @link      https://seothemes.com/themes/corporate-pro
 * @author    SEO Themes
 * @copyright Copyright © 2019 SEO Themes
 * @license   GPL-3.0-or-later
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


//* Load custom style sheet
// add_action( 'wp_enqueue_scripts', 'custom_load_custom_style_sheet', 150 );
// function custom_load_custom_style_sheet() {
// 	wp_enqueue_style( 'custom-stylesheet', CHILD_URL . '/custom.css', array(), PARENT_THEME_VERSION );
// }


// Load Genesis Framework (do not remove).
require_once get_template_directory() . '/lib/init.php';

// Load setup functions.
require_once __DIR__ . '/includes/setup.php';

// Load helper functions.
require_once __DIR__ . '/includes/helpers.php';

// Load scripts and styles.
require_once __DIR__ . '/includes/enqueue.php';

// Load general functions.
require_once __DIR__ . '/includes/general.php';

// Load widget areas.
require_once __DIR__ . '/includes/widgets.php';

// Load hero section.
require_once __DIR__ . '/includes/hero.php';

// Load Customizer settings.
require_once __DIR__ . '/includes/customize.php';

// Load default settings.
require_once __DIR__ . '/includes/defaults.php';

// Load recommended plugins.
require_once __DIR__ . '/includes/plugins.php';

remove_action( 'genesis_before', 'corporate_hero_section_setup' );
remove_action( 'corporate_hero_section', 'genesis_do_posts_page_heading' );
remove_action( 'corporate_hero_section', 'genesis_do_date_archive_title' );
remove_action( 'corporate_hero_section', 'genesis_do_taxonomy_title_description' );
remove_action( 'corporate_hero_section', 'genesis_do_author_title_description' );
remove_action( 'corporate_hero_section', 'genesis_do_cpt_archive_title_description' );


