<?php
/**
 * Template Name: Post Archive
 */

add_action( 'genesis_loop', 'custom_category_loop' );
/**
 * Custom loop that display a list of categories with corresponding posts.
 */
function custom_category_loop() {
 // Grab all the categories from the database that have posts.
    $categories = get_terms( 'category', 'orderby=name&order=ASC');
    // Loop through categories
    foreach ( $categories as $category ) {
        // Display category name
        // echo '<h2 class="post-title">' . $category->name . '</h2>';
        echo '<div class="post-list">';
        // WP_Query arguments
        $args = array(
        'cat' => $category->term_id,
        'orderby' => 'term_order',
        );
        // The Query
        $query = new WP_Query( $args );
        // The Loop
        if ( $query->have_posts() ) {
            while ( $query->have_posts() ) {
                $query->the_post();
                ?>
                <div class="postitem one-half">
                    <small><?php echo get_the_date(); ?></small>
                    <a href="<?php the_permalink();?>">
                    <h3><?php the_title(); ?></h3>
                    <p>
                        <?php the_post_thumbnail( 'thumbnail' , array( 'class' => 'alignleft' )); ?>
                        <?php the_excerpt(); ?>
                    </p>
                    </a>
                </div>
                <?php
            } // End while
        } // End if
    echo '</div>';
    echo '<div class=”clear”></div>';
    // Restore original Post Data
    wp_reset_postdata();
    } // End foreach
}
// Start the engine.
genesis();